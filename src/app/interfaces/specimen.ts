export interface Specimen {
    name: string;
    code: string;
    id: string;
}

export interface SpecimenDTO {
    patientId: string;
    practitionerId: string;
    name: string;
    code: string;
}

export interface SpecimenData {
    code: string;
    name: string;
}

export const specimensData: SpecimenData[] = [
    {
        code: 'BLDV',
        name: 'Blood venous'
    }
];