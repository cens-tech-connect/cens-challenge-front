export interface Patient {
    id: string;
    identifier: string;
    name: string;
    familyName: string;
    fullName?: string;
    secondSurName?: string;
    birthdate: string;
    sex: string;
}

export interface Gender {
    value: string;
    viewValue: string;
}

// male | female | other | unknown
export const PatientGenders: Gender[] = [
    { value: 'female', viewValue: 'Femenino' },
    { value: 'male', viewValue: 'Masculino' },
    { value: 'other', viewValue: 'Otro' },
    { value: 'unknown', viewValue: 'Desconocido' }
];
