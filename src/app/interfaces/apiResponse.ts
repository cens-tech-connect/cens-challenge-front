export interface APIResponse {
    data: any;
    status: boolean;
    message: string;
}