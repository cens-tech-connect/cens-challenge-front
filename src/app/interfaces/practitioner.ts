export interface Practitioner {
    id: string;
    identifier: string;
    name: string;
    familyName: string;
    secondSurName: string;
    birthdate: string;
    sex: string;
}