export interface DiagnosticReport {
    id: string;
    issued: string;
    code: string;
}

export interface DiagnosticReportDTO {
    coding: { code: string; };
    issued: string;
    result: { observationId: string; }[];
    patientId: string;
    specimens: { specimenId: string }[];
    perfomerId: string;
    interpreterId: string;
}
