export interface Observation {
    id?: string;
    coding: {
        code: string,
        display: string
    };
    patientId?: string;
    specimentId?: string;
    valueQuantity?: {
        unit: string,
        value: number,
    };
    lowReferenceRange: {
        unit: string,
        value: number
    };
    highReferenceRange: {
        unit: string,
        value: number
    };
}

export interface TestProfile {
    code: string;
    display: string;
}

export const testProfiles: TestProfile[] = [
    {
        code: '24331-1',
        display: 'Perfil Lípidico'
    },
    {
        code: '24325-3',
        display: 'Perfil Hepático'
    }
];
