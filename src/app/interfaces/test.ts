export interface Test {
    observationId?: string;
    identifier?: string;
    code: string;
    display: string;
    text: string;
    linkId?: string;
    unit?: string;
    lowReference?: string;
    highReference?: string;
    value?: string;
}
