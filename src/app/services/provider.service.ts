import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { retry, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { ErrorService } from '../services/error.service';
import { Patient } from '../interfaces/patient';
import { Practitioner } from '../interfaces/practitioner';

@Injectable({
    providedIn: 'root',
})
export class ProviderService {

    constructor(
        private http: HttpClient,
        private errorService: ErrorService
    ) { }

    private baseUrl = `${environment.apiUrl}/provider`;

    httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
    };

    getPractitionerByRUN(run: string): Observable<Practitioner> {
        let params = new HttpParams();
        params = params.append('run', run);
        return this.http
            .get<Practitioner>(`${this.baseUrl}/practitioner`, { params: params })
            .pipe(catchError(this.errorService.errorHandler));
    }

}
