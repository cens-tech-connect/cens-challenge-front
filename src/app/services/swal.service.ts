import { Injectable } from '@angular/core';
import swal from 'sweetalert2';

@Injectable({
    providedIn: 'root',
})
export class SwalService {

    constructor() { }

    showInfo(text: string, title?: string): void {
        swal.fire({
            title: title ? title : 'Información',
            text: text,
            icon: 'info',
            position: 'center',
            confirmButtonText: 'OK',
            customClass: {
                container: 'custom-swal-container'
            }
        });
    }

    showWarning(text: string, title?: string): void {
        swal.fire({
            title: title ? title : 'Advertencia',
            text: text,
            icon: 'warning',
            position: 'center',
            confirmButtonText: 'OK',
            customClass: {
                container: 'custom-swal-container'
            }
        });
    }

    showError(text: string, title?: string): void {
        swal.fire({
            title: title ? title : 'Error',
            text: text,
            icon: 'error',
            position: 'center',
            confirmButtonText: 'OK',
            customClass: {
                container: 'custom-swal-container'
            }
        });
    }

    showSuccess(text: string, title?: string): void {
        swal.fire({
            title: title ? title : 'Bien !',
            text: text,
            icon: 'success',
            position: 'center',
            confirmButtonText: 'OK',
            customClass: {
                container: 'custom-swal-container'
            }
        });
    }

}
