import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { ErrorService } from '../services/error.service';
import { Test } from '../interfaces/test';

@Injectable({
    providedIn: 'root',
})
export class TerminologyService {

    constructor(
        private http: HttpClient,
        private errorService: ErrorService
    ) { }

    private baseUrl = `${environment.apiUrl}/terminology`;

    httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
    };

    findTestByProfileCode(code: string): Observable<Test[]> {
        return this.http
            .get<Test[]>(`${this.baseUrl}/profile/${code}`)
            .pipe(catchError(this.errorService.errorHandler));
    }

}
