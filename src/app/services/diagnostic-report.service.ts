import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { ErrorService } from '../services/error.service';
import { DiagnosticReport, DiagnosticReportDTO } from '../interfaces/diagnosticReport';
import { APIResponse } from '../interfaces/apiResponse';

@Injectable({
    providedIn: 'root',
})
export class DiagnosticReportService {

    constructor(
        private http: HttpClient,
        private errorService: ErrorService
    ) { }

    private baseUrl = `${environment.apiUrl}/diagnostic-report`;

    httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
    };


    getBypatientId(patientId: string): Observable<DiagnosticReport[]> {
        let params = new HttpParams();
        params = params.append('patientId', patientId);
        return this.http
            .get<DiagnosticReport[]>(`${this.baseUrl}/patient`, { params: params })
            .pipe(catchError(this.errorService.errorHandler));
    }

    addDiagnosticReport(diagnostic: DiagnosticReportDTO): Observable<APIResponse> {
        return this.http
            .post<APIResponse>(this.baseUrl, diagnostic)
            .pipe(catchError(this.errorService.errorHandler));
    }


}
