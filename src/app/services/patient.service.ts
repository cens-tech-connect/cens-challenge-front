import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { ErrorService } from '../services/error.service';
import { Patient } from '../interfaces/patient';

@Injectable({
    providedIn: 'root',
})
export class PatientService {

    constructor(
        private http: HttpClient,
        private errorService: ErrorService
    ) { }

    private baseUrl = `${environment.apiUrl}/patient`;

    httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
    };


    getByRUN(run: string): Observable<Patient> {
        let params = new HttpParams();
        params = params.append('run', run);
        return this.http
            .get<Patient>(this.baseUrl, { params: params })
            .pipe(catchError(this.errorService.errorHandler));
    }
}
