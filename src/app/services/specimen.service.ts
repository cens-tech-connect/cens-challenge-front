import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { ErrorService } from '../services/error.service';
import { Specimen, SpecimenDTO } from '../interfaces/specimen';

@Injectable({
    providedIn: 'root',
})
export class SpecimenService {

    constructor(
        private http: HttpClient,
        private errorService: ErrorService
    ) { }

    private baseUrl = `${environment.apiUrl}/specimen`;

    httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
    };

    getByPatientId(id: string): Observable<Specimen[]> {
        return this.http
            .get<Specimen[]>(`${this.baseUrl}/patient/${id}`)
            .pipe(catchError(this.errorService.errorHandler));
    }


    createSpecimen(data: SpecimenDTO): Observable<SpecimenDTO> {
        return this.http
            .post<SpecimenDTO>(this.baseUrl, data, this.httpOptions)
            .pipe(catchError(this.errorService.errorHandler));
    }
}
