import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { ErrorService } from '../services/error.service';
import { Observation } from '../interfaces/observation';
import { APIResponse } from '../interfaces/apiResponse';

@Injectable({
    providedIn: 'root',
})
export class ObservationService {

    constructor(
        private http: HttpClient,
        private errorService: ErrorService
    ) { }

    private baseUrl = `${environment.apiUrl}/observation`;

    httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
    };

    getObservationParameters(id: string): Observable<Observation[]> {
        return this.http
            .get<Observation[]>(`${this.baseUrl}/parameters/${id}`)
            .pipe(catchError(this.errorService.errorHandler));
    }

    getObservationByDiagnosticReport(id: string): Observable<Observation[]> {
        return this.http
            .get<Observation[]>(`${this.baseUrl}/diagnostic-report/${id}`)
            .pipe(catchError(this.errorService.errorHandler));
    }

    addObservation(obs: Observation): Observable<APIResponse> {
        return this.http
            .post<APIResponse>(this.baseUrl, obs)
            .pipe(catchError(this.errorService.errorHandler));
    }

}
