import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Practitioner } from 'src/app/interfaces/practitioner';
import { ProviderService } from 'src/app/services/provider.service';
import swal, { SweetAlertResult } from 'sweetalert2';
import { SpecimenData, specimensData } from 'src/app/interfaces/specimen';
import { SpecimenService } from 'src/app/services/specimen.service';


interface FormData {
  practitioner: Practitioner;
  specimen: SpecimenData;
}

@Component({
  selector: 'app-dialog-new',
  templateUrl: './dialog-new.component.html',
  styleUrls: ['./dialog-new.component.css']
})
export class DialogNewComponent implements OnInit {

  title: string;
  buttonTitle: string;
  specimensData: SpecimenData[] = specimensData;
  patientId;

  formData: FormData = {
    practitioner: {},
    specimen: {},
  } as FormData;


  dialogSpinner: boolean;

  constructor(
    public dialogRef: MatDialogRef<DialogNewComponent>,
    public dialog: MatDialog,
    private readonly specimenService: SpecimenService,
    private readonly providerService: ProviderService,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) { }

  ngOnInit() {
    this.title = this.data['title'];
    this.buttonTitle = this.data['buttonTitle'];
    this.patientId = this.data['patientId'];
    this.dialogSpinner = false;
  }

  async createSample() {
    const dataSpecimen = Object.assign({});
    dataSpecimen.practitionerId = this.formData.practitioner.id;
    dataSpecimen.patientId = this.data['patientId'];
    dataSpecimen.name = this.specimensData.find(sd => sd.code === this.formData.specimen.code).name;
    dataSpecimen.code = this.formData.specimen.code;
    this.dialogSpinner = true;
    const res = await this.specimenService.createSpecimen(dataSpecimen).toPromise();
    this.dialogSpinner = false;
    this.dialogRef.close(true);
  }

  async findPractitioner(evt: any): Promise<void> {
    try {
      const doc = evt && evt.srcElement ? evt.srcElement.value : null;
      if (doc) {
        this.dialogSpinner = true;
        const practitioner: Practitioner = await this.providerService.getPractitionerByRUN(doc).toPromise();
        this.dialogSpinner = false;
        if (practitioner && practitioner.identifier) {
          this.formData.practitioner = practitioner;
        } else {
          swal.fire({
            title: 'RUN no encontrado',
            text: 'No existen coincidencias',
            icon: 'warning',
            position: 'center',
            confirmButtonText: 'OK',
            customClass: {
              container: 'custom-swal-container'
            }
          });
        }
      }
    } catch (error) {
      this.dialogSpinner = false;
      swal.fire({
        title: 'RUN no encontrado',
        text: 'No existen coincidencias',
        icon: 'warning',
        position: 'center',
        confirmButtonText: 'OK',
        customClass: {
          container: 'custom-swal-container'
        }
      });
      this.formData.practitioner.identifier = null;
    }
  }

}
