
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DialogNewComponent } from '../dialog-new/dialog-new.component';
import * as moment from 'moment';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { PatientService } from '../../services/patient.service';
import { Gender, Patient, PatientGenders } from '../../interfaces/patient';
import { ProviderService } from 'src/app/services/provider.service';
import { Practitioner } from 'src/app/interfaces/practitioner';
import { MatSelectChange } from '@angular/material';
import { Observation, TestProfile, testProfiles } from '../../interfaces/observation';
import { SpecimenService } from '../../services/specimen.service';
import { Specimen } from '../../interfaces/specimen';
import { ObservationService } from 'src/app/services/observation.service';
import { SwalService } from '../../services/swal.service';
import { DiagnosticReportDTO } from 'src/app/interfaces/diagnosticReport';
import { DiagnosticReportService } from '../../services/diagnostic-report.service';

export interface TestRegister {
  date: string;
  testId: string;
  sampleId: string;
  identifier: string;
  name: string;
  familyName: string;
}

interface FormData {
  patient: Patient;
  practitioner: Practitioner;
  resultsDate: Date;
  profile: TestProfile;
  selectedSpecimenId: string;
  patientSpecimens: Specimen[];
}

@Component({
  selector: 'app-results-entry',
  templateUrl: './results-entry.component.html',
  styleUrls: ['./results-entry.component.css']
})
export class ResultsEntryComponent implements OnInit {

  patientSpinner = false;
  testSpinner = false;
  tableSpinner = false;
  formPatientData: Patient = {} as Patient;
  genders: Gender[] = PatientGenders;
  testProfiles: TestProfile[] = testProfiles;
  formTestData: TestRegister = {} as TestRegister;
  displayedColumns: string[] = ['code', 'name', 'result', 'unit', 'referenceRange'];
  dataSource = new MatTableDataSource<Observation>();
  formData: FormData = {
    patient: {},
    practitioner: {},
    profile: {},
    resultsDate: null,
    patientSpecimens: [],
    selectedSpecimenId: null
  } as FormData;
  observableData = [];
  testAction: boolean;
  testTable: boolean;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    private readonly dialog: MatDialog,
    private readonly patientService: PatientService,
    private readonly providerService: ProviderService,
    private readonly specimenService: SpecimenService,
    private readonly observationService: ObservationService,
    private readonly diagnosticReportService: DiagnosticReportService,
    private readonly swalService: SwalService
  ) { }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.testAction = true;
    this.testTable = false;
  }

  getAgeFromBirthdate(birthdate: Date): number {
    const bdate = moment(birthdate);
    if (bdate.isValid()) {
      return moment().diff(bdate, 'years', false);
    }
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogNewComponent, {
      data: {
        title: 'Crear Muestra',
        patientId: this.formData.patient.id,
        buttonTitle: 'ACEPTAR',
      },
      width: '45%',
      disableClose: true,
      panelClass: 'custom-modalbox',
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (result) {
        this.loadSpecimens(this.formData.patient.id);
      }
    });
  }

  // Evento de buscar paciente
  async findPatient(evt: any): Promise<void> {
    try {
      const patientDoc = evt && evt.srcElement ? evt.srcElement.value : null;
      if (patientDoc) {
        this.patientSpinner = true;
        const patient: Patient = await this.patientService.getByRUN(patientDoc).toPromise();
        this.patientSpinner = false;
        if (patient && patient.identifier) {
          await this.loadSpecimens(patient.id);
          this.formData.patient = patient;
          // validación para habilitar select de exámenes
          if ((this.formData.patient.identifier) && (this.formData.practitioner.identifier)) {
            this.testAction = false;
          }
        } else {
          this.swalService.showWarning(`No existen coincidencias con el RUN [${patientDoc}]`, 'RUN no encontrado');
        }
      }
    } catch (error) {
      this.patientSpinner = false;
      this.swalService.showError(error.message || error);
      this.formData.patient.identifier = null;
    }
  }

  async findPractitioner(evt: any): Promise<void> {
    try {
      const doc = evt && evt.srcElement ? evt.srcElement.value : null;
      if (doc) {
        this.testSpinner = true;
        const practitioner: Practitioner = await this.providerService.getPractitionerByRUN(doc).toPromise();
        if (practitioner && practitioner.identifier) {
          this.formData.practitioner = practitioner;
          // validación para habilitar select de exámenes
          if ((this.formData.patient.identifier) && (this.formData.practitioner.identifier)) {
            this.testAction = false;
          }
        } else {
          this.swalService.showWarning(`No existen coincidencias con el RUN [${doc}]`, 'RUN no encontrado');
        }
        this.testSpinner = false;
      }
    } catch (error) {
      this.testSpinner = false;
      this.formData.practitioner.identifier = null;
      this.swalService.showError(error.message || error);
    }
  }

  async onProfileChange(evt: MatSelectChange): Promise<void> {
    try {
      const id = evt && evt.value ? evt.value : null;
      if (id) {
        this.testSpinner = true;
        this.tableSpinner = true;
        const observations: Observation[] = await this.observationService.getObservationParameters(id).toPromise();
        this.dataSource = new MatTableDataSource<Observation>(observations);
        this.testTable = true;
        this.testSpinner = false;
        this.tableSpinner = false;
      }
    } catch (error) {
      this.testSpinner = false;
      this.tableSpinner = false;
      this.swalService.showError(error.message || error);
    }
  }

  async submit() {
    try {
      console.log('guardar :>> ');
      if (this.validateForm()) {
        this.setLoaders(true);
        const validObs = this.dataSource.data.filter(r => {
          const res = r.valueQuantity.value;
          return res && res.toString().trim() !== '' && !isNaN(r.valueQuantity.value);
        });
        const observations: Observation[] = validObs.map(o => {
          o.patientId = this.formData.patient.id;
          o.specimentId = this.formData.selectedSpecimenId;
          return o;
        });
        console.log('observations :>> ', observations);
        const newObs = await Promise.all(observations.map(o => this.observationService.addObservation(o).toPromise()));
        const obsIds: string[] = newObs.map(res => res.data && res.data['id'] ? res.data['id'] : null).filter(Boolean);
        const diagnosticReport: DiagnosticReportDTO = {
          coding: {
            code: this.formData.profile.code
          },
          issued: moment(this.formData.resultsDate).format('YYYY-MM-DD'),
          result: obsIds.map(r => ({ observationId: r })),
          patientId: this.formData.patient.id,
          perfomerId: this.formData.practitioner.id,
          interpreterId: this.formData.practitioner.id,
          specimens: [{ specimenId: this.formData.selectedSpecimenId }]
        };
        console.log('diagnosticReport :>> ', diagnosticReport);
        const drResponse = await this.diagnosticReportService.addDiagnosticReport(diagnosticReport).toPromise();
        console.log('drResponse :>> ', drResponse);
        if (drResponse && drResponse.data && drResponse.data['id']) {
          this.swalService.showSuccess(`Reporte diagnóstico creado correctamente ID [${drResponse.data['id']}]`);
          this.cleanForm();
        } else {
          this.swalService.showWarning(`Reporte diagnóstico: ${drResponse.data['message']}`);
        }
        this.setLoaders(false);
      }
    } catch (error) {
      this.setLoaders(false);
      this.swalService.showError(error.message || error);
    }
  }

  async loadTests() {
  }

  async loadSpecimens(patientId: string): Promise<void> {
    const specimens: Specimen[] = await this.specimenService.getByPatientId(patientId).toPromise();
    this.formData.patientSpecimens.push(...specimens);
  }

  validateTestResult(): boolean {
    const results = this.dataSource.data.filter(t => {
      const res = t.valueQuantity.value;
      return res && res.toString().trim() !== '';
    });
    return results && results.length > 0;
  }

  existsNaNResult(): boolean {
    const nanRes = this.dataSource.data.filter(r => {
      const res = r.valueQuantity.value;
      return res && res.toString().trim() !== '' && isNaN(r.valueQuantity.value);
    });
    return nanRes && nanRes.length > 0;
  }

  validateForm(): boolean {
    if (!this.formData) {
      this.swalService.showWarning('Falta información');
      return false;
    }
    if (!this.formData.patient || !this.formData.patient.id) {
      this.swalService.showWarning('Ingrese paciente');
      return false;
    }
    if (!this.formData.practitioner || !this.formData.practitioner.id) {
      this.swalService.showWarning('Ingrese informante');
      return false;
    }
    if (!this.formData.resultsDate) {
      this.swalService.showWarning('Ingrese fecha');
      return false;
    }
    if (!this.formData.profile || !this.formData.profile.code) {
      this.swalService.showWarning('Seleccione examen');
      return false;
    }
    if (!this.formData.selectedSpecimenId) {
      this.swalService.showWarning('Seleccione muestra');
      return false;
    }
    if (!this.validateTestResult()) {
      this.swalService.showWarning('No hay resultados ingresados');
      return false;
    }
    if (this.existsNaNResult()) {
      this.swalService.showWarning('Resultados deben ser numéricos');
      return false;
    }
    return true;
  }

  setLoaders(isActive: boolean): void {
    this.testSpinner = isActive;
    this.patientSpinner = isActive;
    this.tableSpinner = isActive;
  }

  cleanForm() {
    this.formData = {
      patient: {} as Patient,
      practitioner: {} as Practitioner,
      profile: {} as TestProfile,
      resultsDate: null,
      patientSpecimens: [],
      selectedSpecimenId: null
    };
    this.testAction = true;
    this.testTable = false;
    this.setLoaders(false);
  }

}
