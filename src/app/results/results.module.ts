import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ResultsRoutingModule } from './results-routing.module';
import { SharedModule } from '../shared/shared.module';
import { ResultsEntryComponent } from './results-entry/results-entry.component';
// import { DialogNewComponent } from './dialog-new/dialog-new.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ResultsRoutingModule
  ],
  declarations: [
    ResultsEntryComponent,
    // DialogNewComponent
  ]
})
export class ResultsModule { }
