import { Component, OnInit, ViewChild } from '@angular/core';
import { NotificationService } from '../../core/services/notification.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material';
import { MatPaginator } from '@angular/material/paginator';


export interface PeriodicElement {
  name: string;
  position: string;
  weight: string;
  symbol: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  { position: '1111-1', name: 'Jose', weight: 'Gómez', symbol: 'M' },
  { position: '1111-2', name: 'Helium', weight: 'McCain', symbol: 'F' },
  { position: '1111-3', name: 'Lithium', weight: 'Peréz', symbol: 'F' },
  { position: '1111-4', name: 'Beryllium', weight: 'Morales', symbol: 'M' },
  { position: '1111-5', name: 'Boron', weight: 'Suarez', symbol: 'M' },
  { position: '1111-6', name: 'Carbon', weight: 'Gómez', symbol: 'M' },
  { position: '1111-7', name: 'Nitrogen', weight: 'Beltrán', symbol: 'M' },
  { position: '1111-8', name: 'Oxygen', weight: 'Silva', symbol: 'F' },
  { position: '1111-9', name: 'Fluorine', weight: 'Urus', symbol: 'F' },
  { position: '1111-K', name: 'Neon', weight: 'López', symbol: 'U' },
];

@Component({
  selector: 'app-patient-list',
  templateUrl: './patient-list.component.html',
  styleUrls: ['./patient-list.component.css']
})
export class PatientListComponent implements OnInit {

  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    private notificationService: NotificationService
  ) { }

  async ngOnInit() {
    this.dataSource.paginator = this.paginator;
    await this.loadData();
  }

  async loadData(): Promise<void> {
    this.dataSource.sort = this.sort;
  }

}
