import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PatientsRoutingModule } from './patients-routing.module';
import { PatientListComponent } from './patients-list/patient-list.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    PatientsRoutingModule
  ],
  declarations: [PatientListComponent]
})
export class PatientsModule { }
