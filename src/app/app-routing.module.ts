import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './core/guards/auth.guard';
// import { DialogNewComponent } from './results/dialog-new/dialog-new.component';

const appRoutes: Routes = [
    {
        path: 'patients',
        loadChildren: './patients/patients.module#PatientsModule',
        canActivate: [AuthGuard]
    },
    {
        path: 'results',
        loadChildren: './results/results.module#ResultsModule',
        canActivate: [AuthGuard]
    },
    {
        path: 'reports',
        loadChildren: './reports/reports.module#ReportsModule',
        canActivate: [AuthGuard]
    },
    {
        path: 'about',
        loadChildren: './about/about.module#AboutModule',
        canActivate: [AuthGuard]
    },
    {
        path: '**',
        redirectTo: 'results',
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [RouterModule],
    providers: [],
    /*declarations: [
        DialogNewComponent
    ]*/
})
export class AppRoutingModule { }
