import { Component, OnInit, ViewChild } from '@angular/core';
import { NotificationService } from '../../core/services/notification.service';
import * as moment from 'moment';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { Observation, TestProfile, testProfiles } from '../../interfaces/observation';
import { Gender, Patient, PatientGenders } from 'src/app/interfaces/patient';
import { PatientService } from '../../services/patient.service';
import { DiagnosticReport } from 'src/app/interfaces/diagnosticReport';
import { DiagnosticReportService } from 'src/app/services/diagnostic-report.service';
import { ObservationService } from 'src/app/services/observation.service';
import { SwalService } from '../../services/swal.service';

export interface Section {
    name: string;
    updated: Date;
}

export interface PeriodicElement {
    position: number;
    analysis: string;
    result: string;
    referenceRange: string;
    value: string;
}

interface FormData {
    patient: Patient;
    reports: DiagnosticReport[];
    observations: Observation[];
}

const observations: Observation[] = [];

@Component({
    selector: 'app-reports-finder',
    templateUrl: './reports-finder.component.html',
    styleUrls: ['./reports-finder.component.css']
})
export class ReportsFinderComponent implements OnInit {

    genders: Gender[] = PatientGenders;
    notes: Section[] = [
        { name: 'Perfil Lipídico', updated: new Date('2/20/20') },
        { name: 'Perfil Hepático', updated: new Date('1/18/20') },
    ];
    displayedColumns: string[] = ['code', 'name', 'result', 'unit', 'referenceRange'];
    dataSource = new MatTableDataSource<Observation>();
    formData: FormData = {
        patient: {},
        reports: [],
        observations: []
    } as FormData;
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    readonly testProfiles: TestProfile[] = testProfiles;
    patientSpinner = false;
    reportsSpinner = false;
    tableSpinner = false;

    constructor(
        private notificationService: NotificationService,
        private readonly patientService: PatientService,
        private readonly diagnosticService: DiagnosticReportService,
        private readonly observationServce: ObservationService,
        private readonly swalService: SwalService
    ) { }

    ngOnInit() {
        this.dataSource.paginator = this.paginator;
    }

    getAgeFromBirthdate(birthdate: Date): number {
        const bdate = moment(birthdate);
        if (bdate.isValid()) {
            return moment().diff(bdate, 'years', false);
        }
    }

    async onSearchEvent(evt: any): Promise<void> {
        const patientDoc = evt && evt.srcElement ? evt.srcElement.value : null;
        if (patientDoc) {
            await this.load(patientDoc);
        }
    }

    async load(patientDoc: string): Promise<void> {
        try {
            this.patientSpinner = true;
            this.reportsSpinner = true;
            const pat = await this.loadPatient(patientDoc);
            if (pat && pat.id) {
                try {
                    this.formData.patient = pat;
                    const reports = await this.loadReports(pat.id);
                    if (reports && reports.length > 0) {
                        this.formData.reports = reports;
                    } else {
                        this.swalService.showWarning(`No se encontraron reportes diagnósticos para el paciente [${pat.identifier}]`);
                        this.cleanForm();
                    }
                } catch (error) {
                    this.swalService.showWarning(`No se encontraron reportes diagnósticos para el paciente [${pat.identifier}]`);
                    this.cleanForm();
                }
            } else {
                this.swalService.showWarning('Paciente no encontrado');
            }
            this.patientSpinner = false;
            this.reportsSpinner = false;
        } catch (error) {
            this.patientSpinner = false;
            this.reportsSpinner = false;
            this.swalService.showError(error.message || error);
        }

    }

    async loadObservations(diagnosticReportId: string): Promise<void> {
        try {
            this.tableSpinner = true;
            const obs: Observation[] = await this.observationServce
                .getObservationByDiagnosticReport(diagnosticReportId).toPromise();
            if (obs && obs.length > 0) {
                this.dataSource.data = obs;
                // this.dataSource.data = [];
                // this.dataSource.data.push(...obs);
            } else {
                alert(`No se encontraron observaciones`);
            }
            this.tableSpinner = false;
        } catch (error) {
            this.tableSpinner = false;
            this.swalService.showError(error.message || error);
        }
    }


    async loadReports(patientId): Promise<DiagnosticReport[]> {
        return await this.diagnosticService.getBypatientId(patientId).toPromise();
    }


    getProfileName(code: string): string {
        switch (code) {
            case '24331-1':
                return 'Perfil lípidico';
            case '24325-3':
                return 'Perfil Hepático';
            default:
                return code;
        }
    }

    async loadPatient(patientDoc: string): Promise<Patient> {
        try {
            const patient: Patient = await this.patientService.getByRUN(patientDoc).toPromise();
            if (patient && patient.identifier) {
                return patient;
            } else {
                return null;
            }
        } catch (error) {
            this.swalService.showError(error.message || error);
        }
    }

    async cleanForm() {
        this.formData = {
            observations: [],
            reports: [],
            patient: {} as Patient
        }
    }
}
